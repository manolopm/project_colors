require_dependency 'application_helper'

module ApplicationJumpBoxPatch
  def self.included(base)
    base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable
      alias_method_chain :project_tree_options_for_select, :color
      alias_method_chain :render_projects_for_jump_box, :color
    end
  end

  module InstanceMethods
    def render_projects_for_jump_box_with_color(projects, selected=nil)
      jump = params[:jump].presence || current_menu_item
      s = ''.html_safe
      project_tree(projects) do |project, level|
        padding = level * 16
        text = content_tag('span', project.name, :style => "padding-left:#{padding}px;")
	project_safe = project.identifier.downcase.tr(" ", "_")
        s << link_to(text, project_path(project, :jump => jump), :title => project.name, :class => (project == selected ? 'selected '+ Setting.plugin_project_colors[project_safe] : Setting.plugin_project_colors[project_safe]))
      end
      s
    end

    def project_tree_options_for_select_with_color(projects, options = {})
      s = ''
      project_tree(projects) do |project, level|
        name_prefix = (level > 0 ? '&nbsp;' * 2 * level + '&#187; ' : '').html_safe
        tag_options = {:value => project.id}
        project_safe = project.identifier.downcase.tr(" ","_")
        tag_options[:class] = Setting.plugin_project_colors[project_safe]
        if project == options[:selected] || (options[:selected].respond_to?(:include?) && options[:selected].include?(project))
          tag_options[:selected] = 'selected'
        else
          tag_options[:selected] = nil
        end
        tag_options.merge!(yield(project)) if block_given?
        s << content_tag('option', name_prefix + h(project), tag_options)
      end
      s.html_safe
    end
  end
end


ApplicationHelper.send(:include, ApplicationJumpBoxPatch)
